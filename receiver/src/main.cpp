#include <Arduino.h>

#include <MD_MAX72xx.h>

#include "pindefs.hpp"
#include <stdint.h>


int constexpr N_SAMPLES = 128;
int constexpr IR_OFFSET =  (8 * 4);
int constexpr DISPLAY_UPDATE_DELAY = 0; // ms

void set_display(unsigned long val, uint8_t offset);

// 3 * 4 MD_MAX72XX Square led arrays
MD_MAX72XX display(MD_MAX72XX::FC16_HW, PIN_SPI_MOSI, PIN_SPI_SCK, PIN_SPI_SS, (3 * 4));

int val = 0;

long last = 0;
long displayCounter = DISPLAY_UPDATE_DELAY;

void setup() {
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);

  // Setup SPI display connection
  display.begin();
  display.control(MD_MAX72XX::INTENSITY, 4);
  display.control(MD_MAX72XX::UPDATE, MD_MAX72XX::OFF);
  delay(100);

  last = millis();
  display.clear();
}

void set_display(unsigned long val, uint8_t offset)
{
  // 4 * Columns per square, 4 squares per
  int columns = (8 * 4) * val / 1024;

  // Uses a chequerboard pattern to save on power
  uint8_t pattern = 0xaa;
  for (int i = 0; i != columns; ++i)
  {
    display.setColumn(offset + i, pattern);
    pattern = ~pattern;
  }
}

void loop() {
  long now = millis();
  long delta = now - last;
  last = now;

  displayCounter -= delta;

  if (displayCounter < 0) {
    display.clear();

    set_display(analogRead(A0), 0);
    set_display(analogRead(A1), IR_OFFSET);

    display.update();

    displayCounter += DISPLAY_UPDATE_DELAY;
  }
}
