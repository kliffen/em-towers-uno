#include <Arduino.h>

#include "pindefs.hpp"

uint8_t cur_state = 0;
uint8_t change = 0;

uint8_t const RELAYS[] = {RELAY_1, RELAY_2, RELAY_3, RELAY_4};

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(RELAY_1, OUTPUT);
  pinMode(RELAY_2, OUTPUT);
  pinMode(RELAY_3, OUTPUT);
  pinMode(RELAY_4, OUTPUT);

  // Default everything off
  digitalWrite(LED_BUILTIN, LOW);
  digitalWrite(RELAY_1, LOW);
  digitalWrite(RELAY_2, LOW);
  digitalWrite(RELAY_3, LOW);
  digitalWrite(RELAY_4, LOW);

}

// Simple loop, toggling every relay in a sequence
void loop() {

  if (cur_state & (1 << change))
    digitalWrite(RELAYS[change], LOW);
  else
    digitalWrite(RELAYS[change], HIGH);

  cur_state = cur_state ^ (1 << change);
  change = (change + 1) % 4;

  delay(1000);
}
