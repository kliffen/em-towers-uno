#ifndef PINDEFS_H
#define PINDEFS_H

#include <stdint.h>

uint8_t constexpr RELAY_1 = 12;
uint8_t constexpr RELAY_2 = 11;
uint8_t constexpr RELAY_3 = 10;
uint8_t constexpr RELAY_4 = 9;

#endif // PINDEFS_H
