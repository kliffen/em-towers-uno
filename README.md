# EM Towers Software

This repository contains the software for the 2 Arduino UNO's used in the EM towers.
It uses [PlatformIO](https://platformio.org/) for building and uploading the firmware.


## License

Apache License 2.0
